# Diagrams as Code

## Quick Start

1. Follow installation instructions here: https://diagrams.mingrammer.com/
2. Download my examples to your working folder
3. Execute with pythom
``` shell
python filename.py
```
4. View the generated digram
