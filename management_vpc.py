from diagrams import Cluster, Diagram
from diagrams.aws.network import VPC, PrivateSubnet, PublicSubnet, RouteTable, InternetGateway, NATGateway, TransitGateway, VPCPeering, ELB, RouteTable
from diagrams.aws.compute import EC2, ECS, AutoScaling
from diagrams.aws.security import CloudHSM
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.logging import Fluentd

graph_attr = {
    "fontname": "Arial"
}
with Diagram("Management VPC Overview", show=True, direction="BT", graph_attr=graph_attr):
    with Cluster("Account"):
        with Cluster("Management VPC"):
            VPC()
            RouteTable("Route Table")
            IG = InternetGateway("Internet Gateway")
            VPCPeering("VPC Peering")
            TransitGateway("TRansit Gateway")
            with Cluster("Public Subnet"):
                RouteTable("Route Table")
                PublicSubnet("Public Subnet")
                SQUID = ELB("Internet Egress")
                NG = NATGateway("NAT Gateway")
            with Cluster("Private Subnet"):
                PrivateSubnet("Private Subnet")
                RouteTable("Route Table")
                NLB = ELB("Network Load Balancer")
                with Cluster("Management Services"):
                    ECS("ECS")
                    AutoScaling("AutoScaling")
                    with Cluster("Tasks"):
                        CloudHSM("CloudHSM")
                        Prometheus("Prometheus")
                        Grafana("Grafana")
                        Fluentd("FluentD")

    SQUID >> IG
    NG >> IG
    NLB >> SQUID
    NLB >> NG