from diagrams import Cluster, Diagram
from diagrams.aws.network import VPC, PrivateSubnet
from diagrams.aws.analytics import ES
from diagrams.aws.storage import S3
from diagrams.aws.compute import EC2, ECS
from diagrams.onprem.logging import Fluentd
from diagrams.onprem.monitoring import Grafana, Prometheus

graph_attr = {
    "fontname": "Arial"
}
with Diagram("Log Monitoring", show=True, direction="BT", graph_attr=graph_attr):
    with Cluster("Environment"):
        with Cluster("Logs"):
            storage = S3("Retained Logs")
            metrics = ES("ElasticSearch")
            monitoring = Prometheus("Prometheus")
            visualisation = Grafana("Grafana")
        with Cluster("Management VPC"):
            VPC()
            with Cluster("Aggregator Cluster"):
                PrivateSubnet()
                aggregator = Fluentd("Log Aggregator")
        with Cluster("Environment VPC"):
            VPC()
            with Cluster("Workloads"):
                PrivateSubnet()
                service1 = EC2("EC2 Workload")
                collector1 = Fluentd("Log Collector")
                service2 = ECS("EC2 Workload")
                collector2 = Fluentd("Log Collector")
    service1 >> collector1 >> aggregator
    service2 >> collector2 >> aggregator
    aggregator >> storage
    aggregator >> metrics
    metrics >> monitoring
    monitoring >> visualisation