from diagrams import Cluster, Diagram, Edge
from diagrams.aws.network import VPC, PrivateSubnet, PublicSubnet, RouteTable, InternetGateway, NATGateway, TransitGateway, VPCPeering, ELB, RouteTable, Endpoint, DirectConnect, Route53
from diagrams.aws.compute import EC2, ECS, AutoScaling, Compute, Lambda, ECR
from diagrams.aws.security import CloudHSM, Guardduty, SecretsManager, KMS, IAM
from diagrams.aws.management import Cloudtrail, Config, Cloudwatch
from diagrams.aws.analytics import ES
from diagrams.onprem.monitoring import Grafana, Prometheus, Thanos
from diagrams.onprem.logging import Fluentd
from diagrams.onprem.database import MongoDB
from diagrams.onprem.network import Kong, Nginx, Internet
from diagrams.programming.language import NodeJS, Java
from diagrams.aws.storage import EBS, S3
from diagrams.onprem.security import Trivy, Vault
from diagrams.onprem.iac import Ansible, Terraform
from diagrams.onprem.ci import GitlabCI
from diagrams.aws.integration import SQS, SNS
from diagrams.generic.network import Firewall
from diagrams.generic.device import Tablet
from diagrams.gcp.analytics import Bigquery
from diagrams.generic.place import Datacenter

graph_attr = {
    "fontname": "Arial bold",
    "fontcolor": "blue",
    "center": "true",
    "fontsize": "32"
}
node_attr = {
    "fontname": "Arial bold"
}

# Cloud
with Diagram("DTH2 Platform Architecture", show=True, direction="TB", graph_attr=graph_attr):
    with Cluster("AWS Services"):
        with Cluster("Compute"):
            ECR("ECR"),
            AutoScaling("ASG"),
            ECS("ECS Cluster"),
            EC2("EC2"),
            Lambda("Lambda")
        with Cluster("Security"):
            SecretsManager("Secrets\nManager"),
            Guardduty("GuardDuty"),
            KMS("KMS"),
            IAM("IAM")
        with Cluster("Management"):
            Cloudwatch("CloudWatch"),
            Cloudtrail("CloudTrail"),
            Config("Config")
        with Cluster("Other"):
            ES("ElasticSearch"),
            S3("S3")
    with Cluster("Tools"):
        tools = [
            Vault("Vault"),
            Trivy("Trivy"),
            Ansible("Ansible"),
            Terraform("Terraform"),
            GitlabCI("GitLabCI"),
            Thanos("Thanos")]  
    NGCC = Internet("NGCC")
    SDX = Internet("SDX")
    EXT = Internet("Cloud")
    Akamai = Firewall("Akamai")
    Citizen = Tablet("Citizen")
    Google = Bigquery("Google\nAnalytics")
    DVLA_Service = Datacenter("DVLA")
    with Cluster("AWS Cloud"):
        HCS = VPC("HCS Management\nVPC")
# - Integration VPC
        with Cluster("Integration VPC"):
            IntegrationVPC = VPCPeering("VPC\nPeering")

# - Mongo VPC
        with Cluster("Data Service VPC"):
             MongoVPC = VPCPeering("VPC\nPeering")
             MONGO = MongoDB("Atlas")
# - Account
        with Cluster("Account"):
# -- Audit
            with Cluster("Audit"):
                Audit_S3 = S3()
                with Cluster("Audit\nQueue"):
                    AuditSQS = SQS()
                with Cluster("Create Audit\nEvents"):
                    AuditLAM = Lambda()
                with Cluster("Audit Event\nNotification"):
                    AuditSNS = SNS()
# -- Identity Store
            with Cluster("Identity Store"):
                with Cluster("Event\nQueue"):
                    IdentityStoreSQS = SQS()
                with Cluster("Event\nNotification"):
                    IdentityStoreSNS = SNS()
# -- Management VPC
            with Cluster("Management VPC"):
                ManagementIGW = InternetGateway("Internet\nGateway")
                ManagementVPC = VPCPeering("VPC Peering")
                ManagementTGW = TransitGateway("Transit\nGateway")
# --- Public Subnet
                with Cluster("Public Subnet"):
                    ManagementEgress = ELB("Internet\nEgress")
                    ManagementNAT = NATGateway("NAT\nGateway")

# --- Private Subnet
                with Cluster("Private Subnet"):
                    ManagementNLB = ELB("Management\nNLB")
                    with Cluster("ECS Cluster"):
                        with Cluster("Tasks"):
                            CloudHSM("CloudHSM")
                            Monitoring = Prometheus("Prometheus")
                            Visualisation = Grafana("Grafana")
                            Aggregator = Fluentd("Log\nAggregator")

# -- Transit VPC
            with Cluster("Transit VPC"):
                TransitIGW = InternetGateway("Internet\nGateway")
                TransitVPC = VPCPeering("VPC\nPeering")
                TransitTGW = TransitGateway("Transit\nGateway")

# --- Public Subnet
                with Cluster("Public Subnet"):
                    TransitNAT = NATGateway("NAT\nGateway")

# --- Private Subnet
                with Cluster("Private Subnet"):
                    TransitNLB = ELB("TRansit\nNLB")
                    with Cluster("ECS Cluster"):
                        with Cluster("Tasks"):
                            TransitSquid = Firewall("Transit\nSquid")
                            TransitWAF = Firewall("Transit\nWAF")

# -- Environment VPC
            with Cluster("Environment VPC"):
                DX = DirectConnect("Direct\nConnect")
                EnvironmentVPC = VPCPeering("VPC\nPeering")

# --- Internet Subnet
                with Cluster("Internet Subnet (PVT)"):
                    EnvironmentInternetNLB = ELB("Internet\nNLB")
                    EnvironmentInternetIngressNLB = ELB("Ingress\nNLB")
                    EnvironmentInternetEgressNLB = ELB("Egress\nNLB")
                    with Cluster("ECS Cluster"):
                        with Cluster("Tasks"):
                            EnvironmentInternetReverseProxy = Nginx("Reverse\nProxy")
                            EnvironmentInternetIngressProxy = Firewall("Ingress\nProxy\nSquid")
                            EnvironmentInternetEgressProxy = Nginx("Egress\nProxy")

# --- Services Subnet
                with Cluster("Services Subnet (PVT)"):
                    Route53("Blue/Green")
                    EnvironmentServicesCollector = Fluentd("Log\nCollector")
                    EnvironmentServicesNLB = ELB("Services\nNLB")
                    EnvironmentServicesClusterNLB = ELB("Cluster\nNLB")
                    EnvironmentServicesAMNLB = ELB("AM NLB")
                    EnvironmentServicesIDMNLB = ELB("IDM NLB")
                    EnvironmentServicesGWNLB = ELB("Gateway\nNLB")
                    EnvironmentServicesCXPPaymentsNLB = ELB("Payments\nNLB")
                    EnvironmentServicesCXPInternalNLB = ELB("Internal\nNLB")
                    EnvironmentServicesCXPExternalNLB = ELB("External\nNLB")
                    EnvironmentServicesCXPBFFNLB = ELB("BFF NLB")
                    EnvironmentServicesIdentityStoreAttributeNLB = ELB("Identity Store\nAttribute NLB")
                    EnvironmentServicesIdentityStoreEventsNLB = ELB("Identity Store\nEvents NLB")
                    EnvironmentServicesIdentityStoreScoreNLB = ELB("Identity Store\nScore NLB")
                    EnvironmentServicesIdentityStorePolicyNLB = ELB("Identity Store\nPolicy NLB")
                    EnvironmentServicesDVLANLB = ELB("DVLA\nNLB")
                    EnvironmentServicesMatchingServiceNLB = ELB("Matching\nService NLB")
                    EnvironmentServicesInternalAPIGatewayNLB = ELB("Internal\nAPI GW NLB")
                    EnvironmentServicesKBVMicroNLB = ELB("KBV\nMicroService\nNLB")
                    EnvironmentServicesKBVUIMicroNLB = ELB("KBV UI\nMicroService\nNLB")
                    EnvironmentServicesEBVUIMicroNLB = ELB("EBV UI\nMicroService\nNLB")
                    EnvironmentServicesCredentialMicroNLB = ELB("Credentials\nMicroService\nNLB")
                    EnvironmentServicesMatchingUINLB = ELB("Matching UI\nMicroService\nNLB")
                    EnvironmentServicesUserRegistrationNLB =ELB("UserRegistration\nMicroService\nNLB")
#                   ----------------------------
                    with Cluster("AM Instances"):
                        with Cluster("Instances"):
                            EnvironmentServicesAM = Compute("AM")
                            EnvironmentServicesAMConfig = Compute("AM Config")
                            EnvironmentServicesAMEBS = EBS()
                    with Cluster("IDM Instances"):
                        with Cluster("Instances"):
                            EnvironmentServicesIDM = Compute("IDM")
                    with Cluster("ECS Cluster"):
                        with Cluster("Tasks"):
                            EnvironmentServicesMatchingService = NodeJS("Matching\nService")
                            EnvironmentServicesInternalAPIGateway = Kong("Internal\nAPI GW")
                            EnvironmentServicesKBVMicro = NodeJS("KBV\nMicroService")
                            EnvironmentServicesKBVUIMicro = NodeJS("KBV UI\nMicroService")
                            EnvironmentServicesEBVUIMicro = NodeJS("EBV UI\nMicroService")
                            EnvironmentServicesCredentialMicro = NodeJS("Credentials\nMicroService")
                            EnvironmentServicesMatchingUI = NodeJS("Matching UI\nMicroService")
                            EnvironmentServicesUserRegistration = NodeJS("UserRegistration\nMicroService")
                            EnvironmentServicesCXPPayments = Java("Payment\nMicroservice")
                            EnvironmentServicesCXPInternal = Kong("Internal\nAPI GW")
                            EnvironmentServicesCXPExternal = Kong("External\nAPI GW")
                            EnvironmentServicesCXPBFF = Java("CXP BFF UI")
                            EnvironmentServicesIdentityStoreAttribute = NodeJS("Attribute\nService")
                            EnvironmentServicesIdentityStoreEvents = NodeJS("IDS Events")
                            EnvironmentServicesIdentityStoreScore = NodeJS("IDS Score")
                            EnvironmentServicesIdentityStorePolicy = NodeJS("IDS Policy")
                            EnvironmentServicesDVLA = NodeJS("DVLA")
# --- Data Subnet
                with Cluster("Data Subnet (PVT)"):
                    DataEndpoint = Endpoint()
                    EnvironmentDataCollector = Fluentd("Log\nCollector")
                    S3Backup = S3()
                    with Cluster("ECS Data Cluster"):
                        with Cluster("Tasks"):
                            EnvironmentDataIDM = Compute("IDM")
                            EnvironmentDataUSR = Compute("User")
                            EnvironmentDataCTS = Compute("CTS")
                            EnvironmentDataPOL = Compute("Policy")
                            with Cluster("Volumes"):
                                EnvironmentDataIDM_EBS = EBS("IDM EBS")
                                EnvironmentDataUSR_EBS = EBS("User EBS")
                                EnvironmentDataCTS_EBS = EBS("CTS EBS")
                                EnvironmentDataPOL_EBS = EBS("Policy\nEBS")
                    with Cluster("ECS Replication Cluster"):
                        with Cluster("Tasks"):
                            EnvironmentDataIDM_REP = Compute("IDM\nReplication")
                            EnvironmentDataUSR_REP = Compute("User\nReplication")
                            EnvironmentDataCTS_REP = Compute("CTS\nReplication")
                            with Cluster("Volumes"):
                                EnvironmentDataIDM_REP_EBS = EBS("IDM EBS\nReplication")
                                EnvironmentDataUSR_REP_EBS = EBS("User EBS\nReplication")
                                EnvironmentDataCTS_REP_EBS = EBS("CTS EBS\nReplication")
# Connections
## External
    NGCC << Edge(color="darkgreen", style="bold") >> SDX << Edge(color="darkgreen", style="bold", label="Internal Ingress") >> DX << Edge(color="darkgreen", style="bold") >> EnvironmentInternetNLB << Edge(color="darkgreen", style="bold") >> EnvironmentInternetReverseProxy << Edge(color="darkgreen", style="bold") >> EnvironmentServicesCXPExternalNLB
    EXT >> Edge(color="blue", style="bold") >> Google
    Citizen >> Edge(color="red", style="bold", label="Internet Ingress") >> EXT >> Edge(color="red", style="bold") >> Akamai >> Edge(color="red", style="bold") >> TransitWAF >> Edge(color="red", style="bold") >> EnvironmentInternetIngressNLB >> Edge(color="red", style="bold") >> EnvironmentInternetIngressProxy >> Edge(color="red", style="bold") >> EnvironmentServicesKBVUIMicroNLB >> Edge(color="red", style="bold") >> EnvironmentServicesKBVUIMicro
    EXT << Edge(color="black", style="bold") >> ManagementIGW
    EXT << Edge(color="darkgreen", style="bold") >> TransitIGW
##  Account
    IntegrationVPC << Edge(color="darkgreen", style="bold") >> EnvironmentInternetNLB
    IntegrationVPC << Edge(color="brown", style="bold") >> EnvironmentServicesCXPInternalNLB
    IntegrationVPC << Edge(color="brown", style="bold") >> ManagementVPC
    MongoVPC << Edge(color="black", style="bold") >> EnvironmentInternetNLB
    IntegrationVPC << Edge(color="brown", style="bold") >> EnvironmentVPC
##  Transit VPC
    TransitVPC << Edge(color="darkgreen", style="bold") >> EnvironmentInternetIngressNLB << Edge(color="darkgreen", style="bold") >> EnvironmentInternetNLB
    TransitTGW << Edge(color="black", style="bold") >> HCS
    TransitIGW << Edge(color="black", style="bold") >> TransitNAT
    TransitNAT << Edge(color="black", style="bold") >> TransitNLB
##  Management VPC
    ManagementVPC << Edge(color="black", style="bold") >> MongoVPC << Edge(color="black", style="bold") >> MONGO
    ManagementTGW << Edge(color="black", style="bold") >> HCS
    ManagementVPC << Edge(color="black", style="bold") >> TransitVPC
    ManagementEgress << Edge(color="black", style="bold") >> ManagementIGW
    ManagementNAT << Edge(color="black", style="bold") >> ManagementIGW
    ManagementNLB << Edge(color="black", style="bold") >> ManagementEgress
    ManagementNLB << Edge(color="black", style="bold") >> ManagementNAT
#   Environment VPC
    EnvironmentServicesCollector << Edge(color="black", style="dotted") >> Aggregator << Edge(color="black", style="dotted") >> Monitoring << Edge(color="black", style="dotted") >> Visualisation
    EnvironmentDataCollector << Edge(color="black", style="dotted") >> Aggregator
    EnvironmentVPC << Edge(color="black", style="bold") >> EnvironmentInternetNLB
##  Environment - Internet Subnet
    EnvironmentInternetReverseProxy << Edge(color="darkgreen", style="bold") >> EnvironmentServicesCXPInternalNLB
    EnvironmentInternetEgressProxy << Edge(color="black", style="bold") >> EnvironmentServicesAMNLB
    EnvironmentInternetIngressProxy << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternal
    EnvironmentInternetIngressProxy >> Edge(color="red", style="bold") >> EnvironmentServicesCredentialMicroNLB
    EnvironmentInternetNLB << Edge(color="black", style="bold") >> EnvironmentServicesNLB
##  Environment - Services Subnet
    EnvironmentServicesMatchingService << Edge(color="brown", style="bold") >> IntegrationVPC << Edge(color="brown", style="bold", label="Internal Egress") >> EnvironmentServicesCXPExternal
    EnvironmentServicesDVLA << Edge(color="blue", style="bold") >> EnvironmentInternetEgressNLB << Edge(color="blue", style="bold") >> EnvironmentInternetEgressProxy >> Edge(color="blue", style="bold") >> TransitVPC >> Edge(color="blue", style="bold") >> TransitSquid >> Edge(color="blue", style="bold") >> TransitVPC >> Edge(color="blue", style="bold", label="Internet Egress") >> EXT >> Edge(color="blue", style="bold") >> DVLA_Service
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesClusterNLB << Edge(color="black", style="bold") >> EnvironmentServicesKBVMicro << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternalNLB << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternal << Edge(color="black", style="bold") >> EnvironmentServicesGWNLB << Edge(color="black", style="bold") >> EnvironmentServicesMatchingService << Edge(color="black", style="bold") >> EnvironmentVPC
    EnvironmentServicesMatchingService << Edge(color="black", style="bold") >> EnvironmentServicesNLB << Edge(color="black", style="bold") >> EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternalNLB
    EnvironmentServicesCXPPaymentsNLB << Edge(color="black", style="bold") >> EnvironmentServicesCXPPayments << Edge(color="black", style="bold") >> EnvironmentServicesCXPInternalNLB << Edge(color="black", style="bold") >> EnvironmentServicesCXPInternal << Edge(color="black", style="bold") >> EnvironmentServicesCXPBFFNLB << Edge(color="black", style="bold") >> EnvironmentServicesCXPBFF  
    EnvironmentServicesCXPExternal << Edge(color="black", style="bold") >> EnvironmentServicesCXPBFFNLB    
    EnvironmentServicesKBVUIMicro << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternal
    EnvironmentServicesEBVUIMicro << Edge(color="black", style="bold") >> EnvironmentServicesCXPExternal
    EnvironmentServicesIdentityStoreAttributeNLB << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStoreAttribute
    EnvironmentServicesInternalAPIGateway << Edge(color="darkgreen", style="bold") >> EnvironmentServicesIdentityStoreAttributeNLB
    EnvironmentServicesIdentityStoreAttribute << Edge(color="black", style="bold") >> EnvironmentServicesIDMNLB
    EnvironmentServicesIdentityStoreEventsNLB << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStoreEvents << Edge(color="black", style="bold") >> IdentityStoreSNS
    EnvironmentServicesIdentityStoreAttribute << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStorePolicyNLB << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStorePolicy
    EnvironmentServicesDVLANLB << Edge(color="black", style="bold") >> EnvironmentServicesDVLA
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStorePolicyNLB
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesDVLANLB
    EnvironmentServicesIDM << Edge(color="black", style="bold") >> EnvironmentDataIDM
    EnvironmentServicesIDM << Edge(color="black", style="bold") >> EnvironmentDataUSR
    EnvironmentServicesMatchingServiceNLB << Edge(color="black", style="bold") >> EnvironmentServicesMatchingService 
    EnvironmentServicesInternalAPIGatewayNLB << Edge(color="black", style="bold") >> EnvironmentServicesInternalAPIGateway
    EnvironmentServicesInternalAPIGateway << Edge(color="darkgreen", style="bold") >> EnvironmentServicesAMNLB << Edge(color="darkgreen", style="bold") >> EnvironmentServicesAM
    EnvironmentServicesKBVMicroNLB << Edge(color="black", style="bold") >> EnvironmentServicesKBVMicro
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesKBVMicroNLB
    EnvironmentServicesCredentialMicroNLB << Edge(color="black", style="bold") >> EnvironmentServicesCredentialMicro
    EnvironmentServicesMatchingUINLB << Edge(color="black", style="bold") >> EnvironmentServicesMatchingUI
    EnvironmentServicesMatchingUI << Edge(color="black", style="bold") >> EnvironmentServicesInternalAPIGateway
    EnvironmentServicesUserRegistrationNLB << Edge(color="black", style="bold") >> EnvironmentServicesUserRegistration
    EnvironmentServicesKBVMicro << Edge(color="black", style="bold") >> MongoVPC
    EnvironmentServicesIdentityStoreEvents << Edge(color="black", style="bold") >> MongoVPC
    EnvironmentServicesIdentityStorePolicy << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStoreScoreNLB << Edge(color="black", style="bold") >> EnvironmentServicesIdentityStoreScore << Edge(color="black", style="bold") >> MongoVPC
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesAMConfig << Edge(color="black", style="bold") >> EnvironmentServicesAMEBS
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentDataCTS
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentDataPOL
    EnvironmentServicesAM << Edge(color="black", style="bold") >> EnvironmentServicesIDMNLB << Edge(color="black", style="bold") >> EnvironmentServicesIDM
    EnvironmentServicesKBVMicro << Edge(color="black", style="bold") >> EnvironmentServicesCXPInternalNLB
    EnvironmentServicesGWNLB << Edge(color="black", style="bold") >> EnvironmentServicesInternalAPIGateway
    EnvironmentServicesGWNLB << Edge(color="black", style="bold") >> EnvironmentServicesKBVMicro
    EnvironmentServicesDVLA >> Edge(color="black", style="bold") >> IdentityStoreSNS >> Edge(color="black", style="bold") >> IdentityStoreSQS >> Edge(color="black", style="bold") >> EnvironmentServicesIdentityStoreEventsNLB
    EnvironmentServicesKBVMicro >> Edge(color="black", style="bold") >> IdentityStoreSNS  
##  Environment - Data Subnet
    EnvironmentDataIDM << Edge(color="black", style="bold") >> EnvironmentDataIDM_EBS << Edge(color="black", style="bold") >> EnvironmentDataIDM_REP << Edge(color="black", style="bold") >> EnvironmentDataIDM_REP_EBS << Edge(color="black", style="bold") >> DataEndpoint
    EnvironmentDataUSR << Edge(color="black", style="bold") >> EnvironmentDataUSR_EBS << Edge(color="black", style="bold") >> EnvironmentDataUSR_REP << Edge(color="black", style="bold") >> EnvironmentDataUSR_REP_EBS << Edge(color="black", style="bold") >> DataEndpoint
    EnvironmentDataCTS << Edge(color="black", style="bold") >> EnvironmentDataCTS_EBS << Edge(color="black", style="bold") >> EnvironmentDataCTS_REP << Edge(color="black", style="bold") >> EnvironmentDataCTS_REP_EBS << Edge(color="black", style="bold") >> DataEndpoint
    EnvironmentDataPOL << Edge(color="black", style="bold") >> EnvironmentDataPOL_EBS << Edge(color="black", style="bold") >> DataEndpoint
    DataEndpoint << Edge(color="black", style="bold") >> S3Backup
    EnvironmentDataIDM >> Edge(color="black", style="bold") >> AuditSNS >> Edge(color="black", style="bold") >> AuditSQS >> Edge(color="black", style="bold") >> AuditLAM >> Edge(color="black", style="bold") >> Audit_S3